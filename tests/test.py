import asyncio
import json
import os
import sys
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parent.parent))
from src.pydynalist import Dynalist


def test_sync():
    dyno = Dynalist(os.environ.get("DYNALIST_TOKEN"))
    print(dyno.get_doc_id("Untitled"))


async def test_async():
    dyno = Dynalist(os.environ.get("DYNALIST_TOKEN"))
    print(await dyno.get_doc_id("Untitled"))


def download_doc(title="TreeRo"):
    dyno = Dynalist(os.environ.get("DYNALIST_TOKEN"))
    doc_id = dyno.get_doc_id(title)
    doc = dyno.get_doc(doc_id=doc_id)
    with open(f"{doc_id}.json", "w", encoding="utf-8") as f:
        f.write(json.dumps(doc, indent=4))


def replace(doc_title, old: str, new: str):
    dyno = Dynalist(os.environ.get("DYNALIST_TOKEN"))
    doc_id = dyno.get_doc_id(doc_title)
    doc = dyno.get_doc(doc_id=doc_id)
    changes = []
    for key, value in doc.items():
        if old in value["content"]:
            change = {"action": "edit", "node_id": value["id"], "content": value["content"].replace(old, new)}
            changes.append(change)
        if old in value["note"]:
            change = {"action": "edit", "node_id": value["id"], "note": value["note"].replace(old, new)}
            changes.append(change)

    result = dyno.edit_doc(file_id=doc_id, changes=changes)
    pass


if __name__ == "__main__":
    download_doc()
    # test_sync()
    # asyncio.run(test_async())
